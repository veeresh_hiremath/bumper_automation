package com.bumper.automation;

public class Constants {
		
		//This is the list of System Variables
	    //Declared as 'public', so that it can be used in other classes of this project
	    //Declared as 'static', so that we do not need to instantiate a class object
	    //Declared as 'final', so that the value of this variable can be changed
	    // 'String' & 'int' are the data type for storing a type of value	
		
	    public static final String URL = "http://staging.bumper.com/core/bookings/";
		public static final String Path_TestData = "C://Users//veeresh//workspace//com.bumper.automation//src/test//java//dataEngine//DataEngine.xls";
		public static final String Path_OR = "C://Users//veeresh//workspace//com.bumper.automation//src//test//java//com//bumper//automation//OR.txt";
		public static final String File_TestData = "DataEngine.xlsx";
	 
		//List of Data Sheet Column Numbers
		public static final int Col_TestCaseID = 0;	
		public static final int Col_TestScenarioID =1 ;
		public static final int Col_PageObject = 3;	
		public static final int Col_ActionKeyword =4 ;
		
		public static final int Col_RunMode = 2;
		
		public static final int Col_Result =3 ;
		public static final int Col_TestStepResult =5 ;
		
		public static final String KEYWORD_FAIL = "FAIL";
		public static final String KEYWORD_PASS = "PASS";
	 
		//List of Data Engine Excel sheets
		public static final String Sheet_TestSteps = "Test Steps";
		
		public static final String Sheet_TestCases = "Test Cases";
	 
		// List of Test Data
		public static final String UserPhonenumber = "9886452736";
		public static final String Password = "1234";

}

package com.bumper.automation;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Driver;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.google.gson.Gson;

import executionEngine.DriverScript;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import static executionEngine.DriverScript.OR;

public class ActionKeywords {

	public static AndroidDriver driver = null;
	private static String token;
	private static int id;
	public static int LocationId;
	public static String BookingId;
	public static int DriverId;
	public static int ManagerId;

	public static void openApp(String object) throws MalformedURLException, InterruptedException {

		try {
			File appDir = new File("src/test/java");
			File app = new File(appDir, "app-staging-debug.apk");

			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
			cap.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
			driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
			Thread.sleep(5000);

		} catch (Exception e) {
			System.out.println("Not able to open App");
			DriverScript.bResult = false;
		}
	}

	public static void input_Phonenumber(String object) throws InterruptedException, MalformedURLException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(5000);
			driver.findElement(By.id(OR.getProperty(object))).sendKeys(Constants.UserPhonenumber);
		} catch (Exception e) {
			System.out.println("No phone number");
			DriverScript.bResult = false;
		}
	}

	public void click_Login(String object) throws InterruptedException {
		try {
			// This is fetching the xpath of the element from the Object
			// Repository property file
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(5000);
			driver.findElement(By.id(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Login failed");
			DriverScript.bResult = false;
		}
	}

	public static void input_OTP(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(5000);
			driver.findElement(By.id(OR.getProperty(object))).sendKeys(Constants.Password);
		} catch (Exception e) {
			System.out.println("No OTP number entered");
			DriverScript.bResult = false;
		}
	}

	public static void click_Submit(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.id(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to Submit");
			DriverScript.bResult = false;
		}
	}

	public static void click_Panel(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.id(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fails to select package");
			DriverScript.bResult = false;
		}
	}

	public static void click_Type(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.id(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fails to select panel type");
			DriverScript.bResult = false;
		}
	}
	// @Test(priority=7)
	/*
	 * public static void click_Save(String object) throws InterruptedException
	 * { driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	 * Thread.sleep(3000);
	 * driver.findElement(By.id(OR.getProperty(object))).click(); }
	 */

	// @Test(priority=8)
	public static void click_Next(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.id(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to click Done button");
			DriverScript.bResult = false;
		}
	}

	public static void input_Comment(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(5000);
			driver.findElement(By.id(OR.getProperty(object))).sendKeys("List of Selected Packages");
		} catch (Exception e) {
			System.out.println("Fail to add comments");
			DriverScript.bResult = false;
		}
	}

	// @Test(priority=10)
	public static void click_Schedule(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.xpath(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to click schedule button");
			DriverScript.bResult = false;
		}
	}

	// @Test(priority=11)
	public static void click_Date(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.xpath(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to select date");
			DriverScript.bResult = false;
		}
	}

	public static void click_Time(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.xpath(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to Select time ");
			DriverScript.bResult = false;
		}
	}

	// @Test(priority=13)
	public static void click_Pickup(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.id(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to click PickUp button");
			DriverScript.bResult = false;
		}
	}

	// @Test(priority=14)
	public static void click_Address(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.xpath(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to select Address");
			DriverScript.bResult = false;
		}
	}

	// @Test(priority=15)
	public static void click_Confirm(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.id(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to click confirm");
			DriverScript.bResult = false;
		}
	}

	// @Test(priority=16)
	public static void click_Back(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.id(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to click backward button");
			DriverScript.bResult = false;
		}
	}

	public static void get_BookingId(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			WebElement element = driver.findElement(By.id(OR.getProperty(object)));

			String str = element.getText();

			String split[] = str.split("# ");
			System.out.println("Booking Id = " + split[split.length - 1]);
			BookingId = split[split.length - 1];
			System.out.println(BookingId);

		} catch (Exception e) {
			System.out.println("Fail to get BookingId ");
			DriverScript.bResult = false;
		}
	}

	// @Test(priority=17)
	/*
	 * public static void click_Cancel(String object) throws
	 * InterruptedException { driver.manage().timeouts().implicitlyWait(10,
	 * TimeUnit.SECONDS); Thread.sleep(3000);
	 * driver.findElement(By.xpath(OR.getProperty(object))).click(); }
	 * 
	 * // @Test(priority=18) public static void click_ReasonType(String object)
	 * throws InterruptedException {
	 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 * Thread.sleep(4000); //
	 * driver.findElement(By.xpath(OR.getProperty(object))).click();
	 * driver.findElement(By.xpath(OR.getProperty(object))).click();
	 * 
	 * }
	 * 
	 * // @Test(priority=19) public static void input_Reason(String object)
	 * throws InterruptedException {
	 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 * Thread.sleep(3000);
	 * driver.findElement(By.xpath(OR.getProperty(object))).sendKeys("testing");
	 * }
	 * 
	 * // @Test(priority=20) public static void click_CancelNow(String object)
	 * throws InterruptedException {
	 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 * Thread.sleep(3000);
	 * driver.findElement(By.xpath(OR.getProperty(object))).click(); }
	 * 
	 * // @Test(priority=21) public static void click_Home(String object) throws
	 * InterruptedException { driver.manage().timeouts().implicitlyWait(10,
	 * TimeUnit.SECONDS); Thread.sleep(3000);
	 * driver.findElement(By.id(OR.getProperty(object))).click(); }
	 */
	public static void Login_Api(String object) throws Exception {
		try {

			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "username=9740176267&password=Autoninja1!");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/user/login_with_password/");
			request.post(body);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			Gson gson = new Gson();

			LoginResponse loginResponse = gson.fromJson(response.body().string(), LoginResponse.class);

			token = loginResponse.token;
			System.out.println("Token=" + loginResponse.token);

			System.out.println(response.body().string());

		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.out.println("Fail to call lgoin API");
			DriverScript.bResult = false;
		}
	}

	public static void Driver_Api(String object) throws IOException {
		try {

			OkHttpClient client = new OkHttpClient();

			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/user/?group_name=Driver");
			request.get();
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");

			Response response = client.newCall(request.build()).execute();
			Gson gson = new Gson();

			// System.out.println(response.body().string());

			DriverResponse driverResponse = gson.fromJson(response.body().string(), DriverResponse.class);

			id = driverResponse.results.get(0).id;
			System.out.println("Id=" + id);
			DriverId = id;

		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.out.println("Fail to call Driver API");
			DriverScript.bResult = false;
		}
	}

	public static void Workshop_Api(String object) throws IOException {
		try {

			OkHttpClient client = new OkHttpClient();

			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/master-data/");
			request.get();
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");

			Response response = client.newCall(request.build()).execute();

			Gson gson = new Gson();

			WorkshopResponse workshopResponse = gson.fromJson(response.body().string(), WorkshopResponse.class);

			id = workshopResponse.workshops.get(0).id;
			System.out.println("LocationId=" + id);
			LocationId = id;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.out.println("Fail to call Workshop API");
			DriverScript.bResult = false;
		}
	}

	public static void Workshop_Manager(String object) throws IOException {
		try {
			System.out.println("Workshop manager");
			OkHttpClient client = new OkHttpClient();

			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/user/?group_name=WorkshopManager");
			request.get();
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("content-type", "application/json");

			Response response = client.newCall(request.build()).execute();
			Gson gson = new Gson();
			ManagerResponse managerResponse = gson.fromJson(response.body().string(), ManagerResponse.class);

			id = managerResponse.results.get(0).id;
			System.out.println("ManagerId=" + id);
			ManagerId = id;
		} catch (Exception e) {
			System.out.println("Fail to call Workshop manager API");
			DriverScript.bResult = false;
		}
	}

	public static void Assign_Workshop(String object) throws IOException {
		try {
			// System.out.println("BookingID="+BookingId);
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/json");
			RequestBody body = RequestBody.create(mediaType, "{\"workshop\":" + LocationId + "}");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// request.url("http://staging.bumper.com/api/booking/15239/");
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("content-type", "application/json");
			request.addHeader("cache-control", "no-cache");

			Response response = client.newCall(request.build()).execute();
			// Gson gson= new Gson();

			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call Assign Workshop API");
		}
	}

	public static void Pickup_Driver(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/json");
			RequestBody body = RequestBody.create(mediaType, "{\"pickup_driver\":" + id + "}");// 12124
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// request.url("http://staging.bumper.com/api/booking/15239/");
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("content-type", "application/json");
			request.addHeader("cache-control", "no-cache");

			Response response = client.newCall(request.build()).execute();
			// Gson gson= new Gson();

			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call pickup driver API");
			DriverScript.bResult = false;
		}
	}

	public static void Assign_WorkshopManager(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/json");
			RequestBody body = RequestBody.create(mediaType, "{\"workshop_manager\":" + ManagerId + "}");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// request.url("http://staging.bumper.com/api/booking/15239/");
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("content-type", "application/json");
			request.addHeader("cache-control", "no-cache");

			Response response = client.newCall(request.build()).execute();

			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to Call Assign workshopmanager API");
		}
	}

	// Action APIs
	public static void DriverReady_Go(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=103");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call Driver ready to go API");
			DriverScript.bResult = false;
		}
	}

	public static void TeamReady_Go(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=108");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call Team ready to go API ");
			DriverScript.bResult = false;
		}
	}

	// PICKUP_DRIVER_STARTED = 4
	public static void Started_PickUp(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=4");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call started pickup API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_DRIVER_AT_PICKUP_PLACE = 6
	public static void AtPickUp_Place(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=6");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call At pickup place API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_DRIVER_CREATING_JOB_CARD = 107
	public static void JobCard_Created(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=107");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call Jobcard created API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_CAR_PICKED_UP = 9
	public static void CarPicked_Up(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=9");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call car pickedup API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_CAR_REACHED_WORKSHOP = 10
	public static void Reached_Workshop(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=10");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call Reached workshop API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_CAR_RECEIVED_AT_WORKSHOP = 105
	public static void Received_Workshop(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=105");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call Received workshop API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_WORK_IN_PROGRESS = 13
	public static void Work_Progress(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=13");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call Work progress API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_QUALITY_CHECK_IN_PROGRESS = 14
	public static void Check_Progress(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=14");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call Check progress API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_WORK_COMPLETED = 15
	public static void Work_Completed(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=15");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call work completed API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_PENDING_PAYMENT = 16
	public static void Pending_Payment(String object) throws IOException, InterruptedException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=16");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Fail to call pending payment API");
			DriverScript.bResult = false;
		}
	}

	
	 /* public static void click_PayInApp(String object) throws
	  InterruptedException {
	  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	  Thread.sleep(3000);
	  driver.findElement(By.id("com.bumper.android.staging:id/pay_now_btm_btn")).click(); 
	  }
	 */

	public static void click_Bookings(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.id(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to click Bookings tab");
			DriverScript.bResult = false;
		}
	}

	public static void click_SelectDrop(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.id(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to click Selectdrop");
			DriverScript.bResult = false;
		}
	}

	public static void click_DropDate(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.xpath(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to select Dropdate");
			DriverScript.bResult = false;
		}
	}

	public static void click_DropTime(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.xpath(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to select Droptime");
			DriverScript.bResult = false;
		}
	}

	public static void click_DropLocation(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.xpath(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to click Droplocation");
			DriverScript.bResult = false;
		}
	}

	public static void click_DropAddress(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.xpath(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to select Dropaddress");
			DriverScript.bResult = false;
		}
	}

	// @Test(priority=15)
	public static void click_DropConfirm(String object) throws InterruptedException {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(3000);
			driver.findElement(By.xpath(OR.getProperty(object))).click();
		} catch (Exception e) {
			System.out.println("Fail to click Dropconfirm button");
			DriverScript.bResult = false;
		}
	}

	public static void Drop_Driver(String object) throws IOException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/json");
			RequestBody body = RequestBody.create(mediaType, "{\"drop_driver\":" + id + "}");// 12124
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// request.url("http://staging.bumper.com/api/booking/15361/");
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("content-type", "application/json");
			request.addHeader("cache-control", "no-cache");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
		} catch (Exception e) {
			System.out.println("Fail to call Drop driver API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_DROP_SCHEDULED_DRIVER_READY_TO_GO = 106
	public static void Driver_ReadyToGo(String object) throws IOException, InterruptedException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=106");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			// request.url("http://staging.bumper.com/api/booking/15361/");
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Fail to call Driver ReadyToGo API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_DROP_CAR_ON_THE_WAY = 20
	public static void Started_Workshop(String object) throws IOException, InterruptedException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=20");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			// request.url("http://staging.bumper.com/api/booking/15361/");
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Fail to call StartedWorkshop API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_CAR_REACHED_DROP_LOCATION = 21

	public static void ReachDrop_Location(String object) throws IOException, InterruptedException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=21");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			// request.url("http://staging.bumper.com/api/booking/15361/");
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Fail to call ReachedDropLocation API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_CAR_DELIVERED = 22
	public static void Car_Delivered(String object) throws IOException, InterruptedException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=22");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			// request.url("http://staging.bumper.com/api/booking/15361/");
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Fail to call Cardelivered API");
			DriverScript.bResult = false;
		}
	}

	// ACTION_BOOKING_CLOSED = 23
	public static void Booking_Closed(String object) throws IOException, InterruptedException {
		try {
			OkHttpClient client = new OkHttpClient();

			MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
			RequestBody body = RequestBody.create(mediaType, "action=23");
			Request.Builder request = new Request.Builder();
			request.url("http://staging.bumper.com/api/booking/" + BookingId + "/");
			// need to change to action api
			// request.url("http://staging.bumper.com/api/booking/15361/");
			request.patch(body);
			request.addHeader("authorization", "JWT " + token);
			request.addHeader("cache-control", "no-cache");
			request.addHeader("content-type", "application/x-www-form-urlencoded");

			Response response = client.newCall(request.build()).execute();
			System.out.println(response.body().string());
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Fail to call Booking closed API");
			DriverScript.bResult = false;
		}
	}

}
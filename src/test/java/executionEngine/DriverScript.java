package executionEngine;

import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Properties;
import com.bumper.automation.ActionKeywords;
import com.bumper.automation.Constants;

import utility.ExcelUtils;

public class DriverScript {

	public static Properties OR;
	public static ActionKeywords actionKeywords;

	public static String sActionKeyword;
	// This is reflection class object, declared as 'public static'
	// So that it can be used outside the scope of main[] method
	public static Method method[];
	public static String sPageObject;

	public static int iTestStep;
	public static int iTestLastStep;
	public static String sTestCaseID;
	public static String sRunMode;
	public static boolean bResult;

	public DriverScript() {
		actionKeywords = new ActionKeywords();
		// This will load all the methods of the class 'ActionKeywords' in it.
		// It will be like array of method
		method = actionKeywords.getClass().getMethods();
	}

	public static void main(String[] args) throws Exception
	{
		// Declaring the path of the Excel file with the name of the Excel file
		ExcelUtils.setExcelFile(Constants.Path_TestData);

		// Declaring String variable for storing Object Repository path
		String Path_OR = Constants.Path_OR;

		// Creating file system object for Object Repository property file
		FileInputStream fs = new FileInputStream(Path_OR);

		// Creating an Object of properties
		OR = new Properties(System.getProperties());

		// Loading all the properties from Object Repository property file in to
		// OR object
		OR.load(fs);

		DriverScript startEngine = new DriverScript();
		startEngine.execute_TestCase();
	}

	private void execute_TestCase() throws Exception {
		// This will return the total number of test cases mentioned in the Test
		// cases sheet
		int iTotalTestCases = ExcelUtils.getRowCount(Constants.Sheet_TestCases);
		System.out.println("Total testcases=" + iTotalTestCases);
		// This loop will execute number of times equal to Total number of test
		// cases
		for (int iTestcase = 1; iTestcase <= iTotalTestCases; iTestcase++) {
			//bResult = true;
			// This is to get the Test case name from the Test Cases sheet
			sTestCaseID = ExcelUtils.getCellData(iTestcase, Constants.Col_TestCaseID, Constants.Sheet_TestCases);
			// This is to get the value of the Run Mode column for the current
			// test case
			sRunMode = ExcelUtils.getCellData(iTestcase, Constants.Col_RunMode, Constants.Sheet_TestCases);
			// This is the condition statement on RunMode value
			if (sRunMode.equalsIgnoreCase("Yes")) {
				// Only if the value of Run Mode is 'Yes', this part of code
				// will execute

				iTestStep = ExcelUtils.getRowContains(sTestCaseID, Constants.Col_TestCaseID, Constants.Sheet_TestSteps);

				iTestLastStep = ExcelUtils.getTestStepsCount(Constants.Sheet_TestSteps, sTestCaseID, iTestStep);
				System.out.println("iTestLastStep =" + iTestLastStep);
				System.out.println("output=" + iTestStep);
				// This loop will execute number of times equal to Total number
				// of test steps

				bResult = true;

				sActionKeyword = ExcelUtils.getCellData(iTestStep, Constants.Col_ActionKeyword,
						Constants.Sheet_TestSteps);
				sPageObject = ExcelUtils.getCellData(iTestStep, Constants.Col_PageObject, Constants.Sheet_TestSteps);
				execute_Actions();

				if (!bResult) {
					// If 'false' then store the test case result as Fail
					ExcelUtils.setCellData(Constants.KEYWORD_FAIL, iTestcase, Constants.Col_Result,
							Constants.Sheet_TestCases);

					// By this break statement, execution flow will not
					// execute any more test step of the failed test case
					break;
				}
				// This will only execute after the last step of the test case,
				// if value is not 'false' at any step
				if (bResult) {
					// Storing the result as Pass in the excel sheet
					ExcelUtils.setCellData(Constants.KEYWORD_PASS, iTestcase, Constants.Col_Result,
							Constants.Sheet_TestCases);
				}
			}
		}
		
	}

	// This is the loop for reading the values of the column 3 (Action Keyword)
	// row by row

	private static void execute_Actions() throws Exception {
		// This is a loop which will run for the number of actions in the Action
		// Keyword class
		// method variable contain all the method and method.length returns the
		// total number of methods

		for (int i = 0; i < method.length; i++) {
			// This is now comparing the method name with the ActionKeyword
			// value got from excel
			if (method[i].getName().equals(sActionKeyword)) {
				// In case of match found, it will execute the matched method
				method[i].invoke(actionKeywords, sPageObject);
				// Once any method is executed, this break statement will take
				// the flow outside of for loop
				if (bResult) {
					// If the executed test step value is true, Pass the test
					// step in Excel sheet
					ExcelUtils.setCellData(Constants.KEYWORD_PASS, iTestStep, Constants.Col_TestStepResult,
							Constants.Sheet_TestSteps);
					break;
				} else {
					// If the executed test step value is false, Fail the test
					// step in Excel sheet
					ExcelUtils.setCellData(Constants.KEYWORD_FAIL, iTestStep, Constants.Col_TestStepResult,
							Constants.Sheet_TestSteps);
					break;
				}
			}
		}
	}

}